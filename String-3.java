//We'll say that a lowercase 'g' in a string is "happy" if there is another 'g' immediately to its left or right. Return true if all the g's in the given string are happy.

//https://codingbat.com/prob/p198664

class String3 {

public boolean gHappy(String str){

        boolean flag=false;

        if(str.length()==0)return true;

        if(str.contains("ggg")){
        str=str.replaceAll("ggg","");
        flag=true;
        }
        if(str.contains("gg")){
        str=str.replaceAll("gg","");
        flag=true;

        }

        //  System.out.println(str);

        for(int i=0;i<str.length();i++){


        char c=str.charAt(i);
        if((c=='g')&&(i!=str.length())){

        flag=false;
        break;

        }
        }
        if(flag)return true;
        else return false;

        }
        }
