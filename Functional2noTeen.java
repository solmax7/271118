import java.util.ArrayList;
import java.util.List;

public class Functional2noTeen {


    //        Given a list of integers, return a list of those numbers, omitting any that are between 13 and 19 inclusive.
//
//          https://codingbat.com/prob/p137274
//            noTeen([12, 13, 19, 20]) → [12, 20]
//        noTeen([1, 14, 1]) → [1, 1]
//        noTeen([15]) → []

    public static void main(String[] args) {

        List<Integer> nums = new ArrayList<Integer>();
        nums.add(12);
        nums.add(13);
        nums.add(19);
        nums.add(20);

        noTeen(nums);


    }

    public static List<Integer> noTeen(List<Integer> nums) {

        //int[] massiv = {12, 13, 19, 20};

       // List<Integer> newmassiv = new ArrayList<>();

        List<Integer> newmassiv = new ArrayList<>();


        for (int i = 0; i < nums.size(); i++) {

            if (!(nums.get(i) >= 13 && nums.get(i) <= 19)) {

                newmassiv.add(nums.get(i));
            }

        }
        return newmassiv;
    }
}
