public class Logic1RedTicket {

    //   You have a red lottery ticket showing ints a, b, and c, each of which is 0, 1, or 2. If they are all the value 2, the result is 10. Otherwise if they are all the same, the result is 5. Otherwise so long as both b and c are different from a, the result is 1. Otherwise the result is 0.
//
//         https://codingbat.com/prob/p170833
//        redTicket(2, 2, 2) → 10
//        redTicket(2, 2, 1) → 0
//        redTicket(0, 0, 0) → 5

    public static void main(String[] args) {

        int a = 1;
        int b = 2;
        int c = 0;

        redTicket(a,b,c);


    }

    public static int redTicket(int a, int b, int c) {

        int res;

        if (a == 2 && b == 2 && c == 2) {

            System.out.println(10);
            res = 10;

        } else if (a == b && b == c) {

            System.out.println(5);
            res = 5;

        } else if (b != a && c != a) {

            System.out.println(1);
            res = 1;
        } else
            System.out.println(0);
            res = 0;

        return res;
    }
}
